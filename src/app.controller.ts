import { Controller, Get, HttpException, HttpStatus, Param } from '@nestjs/common';
import { HiveScannerService, HiveNodeStatus } from './scanner/hiveScanner.service';
import { HiveEngineScannerService, HiveEngineNodeStatus } from './scanner/hiveEngineScanner.service';
import { HiveEngineHistoryScannerService, HiveEngineHistoryNodeStatus } from './scanner/hiveEngineHistoryScanner.service';
import { RcService } from './rc/rc.service';

export type NodeScore = {
  name: string;
  endpoint: string;
  version?: string | null;
  score: number;
  updated_at: string;
  success: number;
  fail: number;
  features?: string[];
};

const { name, version } = require('../../package.json');

const BEST_NODES_SCORE_THRESHOLD = 100;
const VALID_NODES_SCORE_THRESHOLD = 75;
const MIN_NODES = 5;

// Hive
const fromHiveNodeStatus = (node: HiveNodeStatus): NodeScore => ({
  name: node.name,
  endpoint: node.endpoint,
  version: node.version || '-',
  score: node.score,
  updated_at: node.updated_at,
  success: node.tests.filter(t => t.success).length,
  fail: node.tests.filter(t => !t.success).length,
  features: node.features,
});

// Hive Engine
const fromHiveEngineNodeStatus = (node: HiveEngineNodeStatus): NodeScore => ({
  name: node.name,
  endpoint: node.endpoint,
  version: node.version || '-',
  score: node.score,
  updated_at: node.updated_at,
  success: node.tests.filter(t => t.success).length,
  fail: node.tests.filter(t => !t.success).length,
  features: node.features,
});

// Hive Engine History
const fromHiveEngineHistoryNodeStatus = (node: HiveEngineHistoryNodeStatus): NodeScore => ({
  name: node.name,
  endpoint: node.endpoint,
  version: node.version || '-',
  score: node.score,
  updated_at: node.updated_at,
  success: node.tests.filter(t => t.success).length,
  fail: node.tests.filter(t => !t.success).length,
  features: node.features,
});

@Controller()
export class hiveAppController {
  constructor(
    private readonly scannerService: HiveScannerService,
    private readonly heScannerService: HiveEngineScannerService,
    private readonly hehScannerService: HiveEngineHistoryScannerService,
    private readonly rcService: RcService,
  ) {}

  @Get('ping')
  ping(): string {
    return 'pong';
  }

  @Get('version')
  version(): Object {
    return { name, version };
  }

  @Get('best')
  best(): NodeScore[] {
    const nodes = this.scannerService.getNodes().filter(n => n.score > 0 && !n.website_only);

    const bestNodes: HiveNodeStatus[] = nodes.filter(n => n.score >= BEST_NODES_SCORE_THRESHOLD);

    // if there are at least MIN_NODES with the best score -> return
    if (bestNodes.length >= MIN_NODES) {
      return bestNodes.map(n => fromHiveNodeStatus(n)).sort((a, b) => b.score - a.score);
    }

    // otherwise reduce the filter threshold
    return nodes
      .filter(n => n.score >= VALID_NODES_SCORE_THRESHOLD)
      .map(n => fromHiveNodeStatus(n))
      .sort((a, b) => b.score - a.score);
  }

  @Get('nodes')
  nodes(): NodeScore[] {
    return this.scannerService.getNodes().map(n => fromHiveNodeStatus(n));
  }

  @Get('nodes/:name')
  node(@Param('name') name: string): HiveNodeStatus {
    const node = this.scannerService.getNodes().find(n => n.name === name);
    if (!node) {
      throw new HttpException('API node not found', HttpStatus.BAD_REQUEST);
    }

    return node;
  }

  @Get('he/best')
  hiveEngineBest(): NodeScore[] {
    const nodes = this.heScannerService.getNodes().filter(n => n.score > 0);

    const bestNodes: HiveEngineNodeStatus[] = nodes.filter(n => n.score >= BEST_NODES_SCORE_THRESHOLD);

    // if there are at least MIN_NODES with the best score -> return
    if (bestNodes.length >= MIN_NODES) {
      return bestNodes.map(n => fromHiveEngineNodeStatus(n)).sort((a, b) => b.score - a.score);
    }

    // otherwise reduce the filter threshold
    return nodes
      .filter(n => n.score >= VALID_NODES_SCORE_THRESHOLD)
      .map(n => fromHiveEngineNodeStatus(n))
      .sort((a, b) => b.score - a.score);
  }

  @Get('he/nodes')
  hiveEngineNodes(): NodeScore[] {
    return this.heScannerService.getNodes().map(n => fromHiveEngineNodeStatus(n));
  }

  @Get('he/nodes/:name')
  hiveEngineNode(@Param('name') name: string): HiveEngineNodeStatus {
    const node = this.heScannerService.getNodes().find(n => n.name === name);
    if (!node) {
      throw new HttpException('API node not found', HttpStatus.BAD_REQUEST);
    }

    return node;
  }

  @Get('heh/best')
  hiveEngineHistoryBest(): NodeScore[] {
    const nodes = this.hehScannerService.getNodes().filter(n => n.score > 0);

    const bestNodes: HiveEngineHistoryNodeStatus[] = nodes.filter(n => n.score >= BEST_NODES_SCORE_THRESHOLD);

    // if there are at least MIN_NODES with the best score -> return
    if (bestNodes.length >= MIN_NODES) {
      return bestNodes.map(n => fromHiveEngineHistoryNodeStatus(n)).sort((a, b) => b.score - a.score);
    }

    // otherwise reduce the filter threshold
    return nodes
      .filter(n => n.score >= VALID_NODES_SCORE_THRESHOLD)
      .map(n => fromHiveEngineHistoryNodeStatus(n))
      .sort((a, b) => b.score - a.score);
  }

  @Get('heh/nodes')
  hiveEngineHistoryNodes(): NodeScore[] {
    return this.hehScannerService.getNodes().map(n => fromHiveEngineHistoryNodeStatus(n));
  }

  @Get('heh/nodes/:name')
  hiveEngineHistoryNode(@Param('name') name: string): HiveEngineHistoryNodeStatus {
    const node = this.hehScannerService.getNodes().find(n => n.name === name);
    if (!node) {
      throw new HttpException('API node not found', HttpStatus.BAD_REQUEST);
    }

    return node;
  }

  @Get('rc/costs')
  rc(): any[] {
    const result = this.rcService.getRcCosts();
    if (!result) {
      throw new HttpException('Data not available, try again in a few minutes', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result;
  }

  @Get('rc/history')
  rcHistory(): any[] {
    return this.rcService.getRcHistoryCosts();
  }
}
