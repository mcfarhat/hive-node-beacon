// create string of a size decide by the user
export const sizedMessage = (finalLength: number, str: string) => {
  while (str.length < finalLength) {
    str = str.concat(str);
  }
  return str.slice(0, finalLength);
};

export const calculateAvailableRc = (lastUpdateTime: number, max: string, current: string) => {
  // last_rc + (now - last_update) * max_rc / 5 days
  const elapsed = Date.now() / 1000 - lastUpdateTime;
  const currentRc = parseFloat(current) + (elapsed * parseFloat(max)) / 432000;
  return Math.min(currentRc, parseFloat(max));
};

export const vestToHive = (totalVestingHive: string | number, vestingShares: string | number, totalVestingShare: string | number) => {
  return (parseFloat(`${totalVestingHive}`) * parseFloat(`${vestingShares}`)) / parseFloat(`${totalVestingShare}`) / 10 ** 3;
};
