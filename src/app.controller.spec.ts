import { Test, TestingModule } from '@nestjs/testing';
import { hiveAppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let appControllerHive: hiveAppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [hiveAppController],
      providers: [AppService],
    }).compile();

    appControllerHive = app.get<hiveAppController>(hiveAppController);
  });

  describe('root', () => {
    it('should return "pong"', () => {
      expect(appControllerHive.ping()).toBe('pong');
    });
  });
});
