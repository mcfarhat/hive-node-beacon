import { sizedMessage } from './helperFunctions';

export const nodes = [/*'https://hive-api.arcange.eu', 'https://hived.emre.sh', 'https://api.hive.blog',*/ 'https://api.openhive.network'];

export const message = 'This whole sentence is precisely fifty characters.';

export const permlink = 'beacon-rc-test-number-';

export const voteWeight = 5;

// operations to check the RC
export const generateOperations = accountName => [
  {
    name: 'custom_json_50',
    method: 'custom_json',
    params: {
      id: 'test_rc_custom_json_50',
      json: JSON.stringify(sizedMessage(50, message)),
      required_auths: [],
      required_posting_auths: [accountName],
    },
    active_required: false,
  },
  {
    name: 'custom_json_200',
    method: 'custom_json',
    params: {
      id: 'test_rc_custom_json_200',
      json: JSON.stringify(sizedMessage(200, message)),
      required_auths: [],
      required_posting_auths: [accountName],
    },
    active_required: false,
  },
  {
    name: 'custom_json_1000',
    method: 'custom_json',
    params: {
      id: 'test_rc_custom_json_1000',
      json: JSON.stringify(sizedMessage(1000, message)),
      required_auths: [],
      required_posting_auths: [accountName],
    },
    active_required: false,
  },
  {
    name: 'custom_json_8000',
    method: 'custom_json',
    params: {
      id: 'test_rc_custom_json_8000',
      json: JSON.stringify(sizedMessage(8000, message)),
      required_auths: [],
      required_posting_auths: [accountName],
    },
    active_required: false,
  },
  {
    name: 'comment_50',
    method: 'comment',
    params: {
      author: accountName,
      title: 'Testing Resource Credits after HF 1.26.0',
      body: sizedMessage(50, message),
      parent_author: 'yozen',
      parent_permlink: 'testing-resource-credits-after-hf-1260',
      permlink: '',
      json_metadata: '"tags":["peakopen"],"app":"peakd/2022.10.1"',
    },
    active_required: false,
  },
  {
    name: `vote_${voteWeight}%`,
    method: 'vote',
    params: {
      voter: accountName,
      author: '',
      permlink: '',
      weight: voteWeight * 100,
    },
    active_required: false,
  },
  {
    name: 'account_witness_vote',
    method: 'account_witness_vote',
    params: {
      account: accountName,
      witness: 'steempeak',
      approve: true,
    },
    active_required: true,
  },
  {
    name: 'account_witness_vote',
    method: 'account_witness_vote',
    params: {
      account: accountName,
      witness: 'steempeak',
      approve: false,
    },
    active_required: true,
  },
  // {
  //   name: 'claim_reward_balance',
  //   method: 'claim_reward_balance',
  //   params: {
  //     account: accountName,
  //     reward_hbd: '0.000 HBD',
  //     reward_hive: '0.000 HIVE',
  //     reward_vests: '0.000001 VESTS',
  //   },
  //   active_required: true,
  // },
  {
    name: 'transfer_without_memo',
    method: 'transfer',
    params: {
      from: accountName,
      to: accountName,
      amount: '0.001 HBD',
      memo: '',
    },
    active_required: true,
  },
  {
    name: 'transfer_with_memo_100',
    method: 'transfer',
    params: {
      from: accountName,
      to: accountName,
      amount: '0.001 HBD',
      memo: sizedMessage(100, message),
    },
    active_required: true,
  },
  {
    name: 'claim_account',
    method: 'claim_account',
    params: {
      creator: accountName,
      fee: '0.000 HIVE',
      extensions: [],
    },
    active_required: true,
  },
];
