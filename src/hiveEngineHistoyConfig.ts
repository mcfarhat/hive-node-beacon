export const hiveEngineHistoryNodes = [
  { name: 'enginehistory.rishipanthee.com', endpoint: 'https://enginehistory.rishipanthee.com/' },
  { name: 'history.hive-engine.com', endpoint: 'https://history.hive-engine.com/' },
];

export const generateTests = () => [
  {
    name: 'get_account_history',
    description: 'Gets user history',
    type: 'fetch',
    endpoint: 'accountHistory',
    params: { account: 'rishi556', limit: 50 },
    score: 10,
    features: ['get_account_history'],
    debug: false,
    validator: result => {
      return result.length === 50;
    },
  },
  {
    name: 'get_market_history',
    description: 'Gets market history',
    type: 'fetch',
    endpoint: 'marketHistory',
    params: { symbol: 'BEE' },
    score: 10,
    features: ['get_account_history'],
    debug: false,
    validator: result => {
      return result.length === 500;
    },
  },
  {
    name: 'get_nft_history',
    description: 'Gets nft history',
    type: 'fetch',
    endpoint: 'nftHistory',
    params: { nfts: [1], symbol: 'CITY' },
    score: 10,
    features: ['get_account_history'],
    debug: false,
    validator: result => {
      return result.length > 1;
    },
  },
];
