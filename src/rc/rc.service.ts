import * as hiveRc from '@hiveio/hive-js-rc';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron } from '@nestjs/schedule';
import { calculateAvailableRc, vestToHive } from 'src/helperFunctions';
import { HiveScannerService, HiveNodeStatus } from '../scanner/hiveScanner.service';
import { hiveNodes } from '../hiveConfig';
import { generateOperations, permlink } from 'src/rcConfig';

const RC_HISTORY_SIZE = 20;

const getResourceCredits = (account: string): Promise<any> => hiveRc.api.callAsync('rc_api.find_rc_accounts', { accounts: [account] });
const getDynamicGlobalProperties = (): Promise<any> => hiveRc.api.callAsync('database_api.get_dynamic_global_properties', {});

const getPost = (author: string, permlink: string) => hiveRc.api.callAsync('bridge.get_post', { author: author, permlink: permlink });

const getLastCreatedPost = (): Promise<any> => hiveRc.api.callAsync('bridge.get_ranked_posts', { sort: 'created', tag: '', observer: '' });

const getBestNode = (nodes: HiveNodeStatus[] = []): string => {
  const validNodes: HiveNodeStatus[] = nodes.filter(n => n.score >= 0 && !n.website_only);

  // if there are at least MIN_NODES with the best score -> return
  if (validNodes.length === 0) {
    return hiveNodes[0].endpoint;
  }

  // otherwise reduce the filter threshold
  return validNodes.sort((a, b) => b.score - a.score).shift().endpoint;
};

@Injectable()
export class RcService {
  private readonly logger = new Logger(RcService.name);

  private isRunning = false;

  private account: string = '';
  private accountPostingKey: string = '';
  private accountActiveKey: string = '';

  private operations: any[] = [];

  private rcHistory: any[] = [];

  constructor(private readonly scannerService: HiveScannerService, private configService: ConfigService) {
    this.account = this.configService.get<string>('RC_ACCOUNT') || 'beacon.testing';
    this.accountPostingKey = this.configService.get<string>('RC_ACCOUNT_POSTING_KEY');
    this.accountActiveKey = this.configService.get<string>('RC_ACCOUNT_ACTIVE_KEY');

    this.operations = generateOperations(this.account);
  }

  onModuleInit() {
    this.run();
  }

  getRcCosts(): any[] {
    return this.rcHistory[0];
  }

  getRcHistoryCosts(): any[] {
    return this.rcHistory;
  }

  @Cron('*/10 * * * *')
  async run() {
    // skip if disabled
    if (this.configService.get<string>('ENABLE_RC', 'true').toLowerCase() !== 'true') {
      this.logger.warn('RC cost checker disabled -> skip');
      return;
    }

    // skip if no posting key
    if (!this.accountPostingKey) {
      this.logger.warn('RC cost checker need a valid account/posting_key -> skip');
      return;
    }

    // skip if already running
    if (this.isRunning) {
      this.logger.warn(`Skip RC run as previous task still running`);
      return;
    }

    this.isRunning = true;

    try {
      const rcHiveNode = getBestNode(this.scannerService.getNodes());
      hiveRc.api.setOptions({ url: rcHiveNode || 'https://api.hive.blog' });
      const dynamicGlobalProperties = await getDynamicGlobalProperties();
      const totalVestingHive = dynamicGlobalProperties.total_vesting_fund_hive.amount;
      const totalVestingShares = dynamicGlobalProperties.total_vesting_shares.amount;

      const startTime = new Date();
      const testsResults = [];
      for (const operation of this.operations) {
        // Try to broadcast => need enough RC
        try {
          if (this.accountPostingKey) {
            this.logger.log(`Cast '${operation.name}', params: ${JSON.stringify(operation.params)}: ...`);

            const resourceCredits = await getResourceCredits(this.account);
            const rcBefore = calculateAvailableRc(
              resourceCredits.rc_accounts[0].rc_manabar.last_update_time,
              resourceCredits.rc_accounts[0].max_rc,
              resourceCredits.rc_accounts[0].rc_manabar.current_mana,
            );

            // set next comment and vote
            if (operation.method === 'comment') {
              const post = await getPost('yozen', 'testing-resource-credits-after-hf-1260');
              operation.params.permlink = `${permlink}${post.children + 1}`;
            } else if (operation.method === 'vote') {
              const lastCreatedPost = await getLastCreatedPost();
              operation.params.author = lastCreatedPost[0].author;
              operation.params.permlink = lastCreatedPost[0].permlink;
            }

            await hiveRc.broadcast.sendAsync(
              {
                operations: [[operation.method, operation.params]],
                extensions: [],
              },
              operation.active_required ? { posting: this.accountActiveKey } : { active: this.accountPostingKey },
            );

            // wait 15 seconds and fetch the RC after the operation
            await new Promise(resolve => setTimeout(resolve, 15000));

            const rcAfter = await getResourceCredits(this.account);
            const rcUsed = Math.round(
              rcBefore - parseFloat(rcAfter.rc_accounts[0].rc_manabar.current_mana) + parseFloat(rcAfter.rc_accounts[0].max_rc) * (10 / 432000),
            ); //assuming 15 seconds in between calls, 5 days for 100% refill
            this.logger.log(`Broadcast '${operation.name}' used ${rcUsed}`);

            const result = {
              operation: operation.name,
              full_operation: operation,
              method: operation.method,
              rc_needed: rcUsed,
              hp_needed: vestToHive(totalVestingHive, rcUsed, totalVestingShares),
              broadcasted: true,
            };
            if (!testsResults.find(test => test.operation === result.operation)) {
              testsResults.push(result);
            }
          } else {
            this.logger.log(`Skip ${operation.name} -> no posting key`);
          }

          // Operation not broadcasted => parse required RC from the error
        } catch (error) {
          this.logger.warn(`Call '${operation.name}', failed: ${error.toString()}`);

          try {
            const rcRequired = error.cause.data.stack[0].data.rc_needed;
            this.logger.log(`Broadcast '${operation.name}' requires ${rcRequired}`);

            const result = {
              operation: operation.name,
              full_operation: operation,
              method: operation.method,
              rc_needed: parseInt(rcRequired),
              hp_needed: vestToHive(totalVestingHive, rcRequired, totalVestingShares),
              broadcasted: false,
            };
            if (!testsResults.find(test => test.operation === result.operation)) {
              testsResults.push(result);
            }
          } catch (ignore) {}

          continue;
        }
      }

      const endTime = new Date();

      const lastTestsCycle = {
        timestamp: endTime,
        costs: testsResults,
      };
      this.rcHistory.unshift(lastTestsCycle);
      if (this.rcHistory.length > RC_HISTORY_SIZE) {
        this.rcHistory.pop();
      }

      // this.logger.log(`Last tests cycle results: ${JSON.stringify(this.rcHistory[0].testsResults, null, 2)}`);
      this.logger.log(`RC costs process started at ${startTime} and ended at ${endTime}`);
    } finally {
      this.isRunning = false;
    }
  }
}
