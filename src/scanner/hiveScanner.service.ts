import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron } from '@nestjs/schedule';
import * as hive from '@hiveio/hive-js';
import { hiveNodes, generateTests } from '../hiveConfig';
import { QuotesService } from '../quotes/quotes.service';

export type HiveNodeTest = {
  name: string;
  description: string;
  type: string;
  method: string;
  params: any;
  score: number;
  features?: string[];
  debug: boolean;
  validator: (result: any) => boolean;
};

export type HiveNodeTestResult = {
  name: string;
  description: string;
  type: string;
  method: string;
  success: boolean;
  features?: string[];
};

export type HiveNodeStatus = {
  name: string;
  endpoint: string;
  version?: string | null;
  updated_at: string;
  score: number;
  features?: string[];
  website_only: boolean;
  tests: HiveNodeTestResult[];
};

export let lastHiveBlock: string;

const promiseWithTimeout = (promise: Promise<any>, timeoutMs: number) => {
  let timeoutHandle: NodeJS.Timeout;
  const timeoutPromise = new Promise((resolve, reject) => {
    timeoutHandle = setTimeout(() => reject(new Error(`Call timeout after ${timeoutMs}ms`)), timeoutMs);
  });

  return Promise.race([promise, timeoutPromise]).then(result => {
    clearTimeout(timeoutHandle);
    return result;
  });
};

@Injectable()
export class HiveScannerService implements OnModuleInit {
  private readonly logger = new Logger(HiveScannerService.name);

  private isRunning = false;

  private tests: HiveNodeTest[] = [];

  private store: HiveNodeStatus[] = [];

  constructor(private configService: ConfigService, private quotesService?: QuotesService) {
    const beaconAccount = this.configService.get<string>('BEACON_ACCOUNT') || 'peak.beacon';

    const apiChainId = this.configService.get<string>('API_CHAIN_ID') || 'beeab0de00000000000000000000000000000000000000000000000000000000';
    const apiParamAccount = this.configService.get<string>('API_PARAM_ACCOUNT') || 'peakd';
    const apiParamCommunity = this.configService.get<string>('API_PARAM_COMMUNITY') || 'hive-189727';

    this.tests = generateTests(apiChainId, apiParamAccount, apiParamCommunity, beaconAccount);
  }

  onModuleInit() {
    this.scan();
  }

  getNodes(): HiveNodeStatus[] {
    return this.store;
  }

  @Cron('0-59/10 * * * *')
  async scan(): Promise<boolean> {
    // skip if disabled
    if (this.configService.get<string>('ENABLE_HIVE', 'true').toLowerCase() !== 'true') {
      this.logger.warn('Scanner (HIVE) disabled -> skip');
      return;
    }

    // skip if already running
    if (this.isRunning) {
      this.logger.warn('Scanner already running -> skip');
      return;
    }

    this.isRunning = true;

    const apiCallTimeout = this.configService.get<number>('API_CALL_TIMEOUT') || 15000;

    try {
      const maxScore: number = this.tests.reduce((acc, cur) => {
        return acc + cur.score;
      }, 0);

      this.logger.log('Starting node scanner ...');

      const excludedNodes = this.configService.get<string>('EXCLUDED_NODES') ? this.configService.get<string>('EXCLUDED_NODES').split(',') : [];

      const nodes = hiveNodes.filter(n => !excludedNodes.includes(n.name));

      this.logger.log('Configured nodes: ' + nodes.map(n => n.name));

      for (const node of nodes) {
        this.logger.log(`Switching node to: ${node.name}`);
        hive.api.setOptions({ url: node.endpoint });

        let score: number = maxScore;
        const results: HiveNodeTestResult[] = [];
        let nodeVersion = null;

        for (const test of this.tests) {
          try {
            if (test.type === 'fetch') {
              this.logger.log(`Call '${test.name}', params: ${JSON.stringify(test.params)}: ...`);

              const start = Date.now();
              const result = await promiseWithTimeout(hive.api.callAsync(test.method, test.params), apiCallTimeout);
              if (test.name === 'get_version') {
                nodeVersion = result.blockchain_version;
              }

              if (test.name === 'dynamic_global_properties') {
                lastHiveBlock = result.head_block_number;
              }

              if (test.debug) {
                this.logger.debug(`Call result: ${JSON.stringify(result)}`);
              }

              const success = test.validator ? test.validator(result) : true;
              const elapsed = Date.now() - start;

              if (success) {
                this.logger.log(`Call '${test.name}', completed in ${elapsed} ms`);
                results.push({
                  name: test.name,
                  description: test.description,
                  type: test.type,
                  method: test.method,
                  success: true,
                  ...(test.features && test.features.length ? { features: test.features } : {}),
                });
              } else {
                this.logger.warn(`Call '${test.name}', failed in ${elapsed} ms. Response: ${JSON.stringify(result)}`);
                score -= test.score;
                results.push({
                  name: test.name,
                  description: test.description,
                  type: test.type,
                  method: test.method,
                  success: false,
                });
              }
            } else if (test.type === 'cast') {
              const start = Date.now();
              let result = null;
              if (test.method === 'custom_json') {
                const postingKey = this.configService.get<string>('BEACON_ACCOUNT_POSTING_KEY');
                if (postingKey) {
                  this.logger.log(`Cast '${test.name}', params: ${JSON.stringify(test.params)}: ...`);
                  result = await hive.broadcast.sendAsync(
                    {
                      operations: [[test.method, test.params]],
                      extensions: [],
                    },
                    { posting: postingKey.trim() },
                  );
                } else {
                  this.logger.log(`Skip ${test.name} -> no posting key`);
                }
              } else if (test.method === 'transfer') {
                const activeKey = this.configService.get<string>('BEACON_ACCOUNT_ACTIVE_KEY');
                if (activeKey) {
                  const params = {
                    ...test.params,
                    memo: this.quotesService.getRandomQuote(),
                  };

                  this.logger.log(`Cast '${test.name}', params: ${JSON.stringify(params)}: ...`);
                  result = await hive.broadcast.sendAsync({ operations: [[test.method, params]], extensions: [] }, { active: activeKey.trim() });
                } else {
                  this.logger.log(`Skip ${test.name} -> no active key`);
                }
              } else {
                throw new Error(`Unsupported cast operation ${test.method}`);
              }

              if (test.debug) {
                this.logger.debug(`Cast result: ${JSON.stringify(result)}`);
              }

              const elapsed = Date.now() - start;

              this.logger.log(`Cast '${test.name}', completed in ${elapsed} ms`);

              results.push({
                name: test.name,
                description: test.description,
                type: test.type,
                method: test.method,
                success: true,
                ...(test.features && test.features.length ? { features: test.features } : {}),
              });
            }
          } catch (error) {
            this.logger.warn(`Call '${test.method}', failed: ${error.toString()}`);
            score -= test.score;

            results.push({
              name: test.name,
              description: test.description,
              type: test.type,
              method: test.method,
              success: false,
            });
          }
        }

        const nodeScore = Math.round((score * 100) / maxScore);
        const nodeFeatures = [...new Set(results.flatMap(r => r.features || []))];

        // updated node status in local store
        const updatedNodeStatus = {
          name: node.name,
          endpoint: node.endpoint,
          version: nodeVersion,
          updated_at: new Date().toISOString(),
          score: nodeScore,
          website_only: node.website_only || false,
          features: nodeFeatures,
          tests: results,
        };

        const prevIndex = this.store.findIndex(stored => stored.name === updatedNodeStatus.name);
        if (prevIndex === -1) {
          this.store.push(updatedNodeStatus);
        } else {
          this.store.splice(prevIndex, 1, updatedNodeStatus);
        }
        this.logger.log(`Node scan completed for ${node.name}, score: ${nodeScore}`);
      }

      this.logger.log('Node scan completed successfully');
      return true;
    } catch (error) {
      this.logger.error(`Unexpected error during node scanning: ${error.toString()}`);
      return false;
    } finally {
      this.isRunning = false;
    }
  }
}
